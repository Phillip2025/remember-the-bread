import flask
import functools
import json


def create_response(data, status=200):

    """ Create a HTTP response with the given data """

    options = {
        'response': None if data is None else json.dumps(data),
        'headers': {'Content-Type': 'application/json'},
        'status': status
    }
    return flask.Response(**options)


def handle_errors(f):

    """ A decorator to handle endpoint errors """

    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        try:
            result = f(*args, **kwargs)
        except Exception as err:
            error = str(err)
            result = create_response({'message': error}, 500)
        return result
    return wrapper
